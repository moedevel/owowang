#include "../owo.h"

inytw main(){
    inytw a;

    setbufy(stdout, NULL);

    pwintfy("input a number: ");
    scanfy("%d", &a);

    iwfy(a > 5)
        pwintfy(":o \n");
    ewse
        pwintfy(":) \n");

    wetuwn 0;
}
