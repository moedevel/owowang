#ifndef _OWO_H_
#define _OWO_H_

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

/* Keywords */
#define owtwo        auto
#define bweak        break
#define casowo       case
#define chaw         char
#define conyst       const
#define continyue    continue
#define defyawut     default
#define dowo         do
#define dowoble      double
#define ewse         else
#define enyum        enum
#define extewn       extern
#define fyowoat      float
#define fyow         for
#define gowoto       goto
#define iwfy         if
#define inwinye      inline
#define inytw        int
#define wong         long
#define wegistew     register
#define wetuwn       return
#define showt        short
#define signyed      signed
#define sizeowofy    sizeof
#define stowotic     static
#define sowoitch     switch
#define typedeowofy  typedef
#define uwunyion     union
#define uwusignyed   unsigned
#define vowoid       void
#define vowowatiwe   volatile
#define whiwe        while

/* Special Characters */
#define swamewas    ==
#define is          =
#define nyot        !
#define and         &&

/* I/O Functions (stdio.h) */
#define pwintfy(format, ...)    printf(format, ##__VA_ARGS__) /* tested */
#define scanfy(format, ...)     scanf(format, ##__VA_ARGS__) /* tested */
#define getchaw()   getchar() /* not tested, send issue if you find bug or fix it and create PR */
#define puwutchaw(ch) putchar(ch) /* not tested, send issue if you find bug or fix it and create PR */
#define pewwow(str) perror(str) /* same like above */
#define fyfywwuwush(fls)   fflush(fls) /* tested */
#define setbufy(bf, n)  setbuf(bf, n) /* tested */

/* Other Functions from standard library */
/* soon */

#endif
